// Dependencies
var express = require('express')
var router = new express.Router()
var jwt = require('jsonwebtoken')

var User = require('models/user.model')
var config = require('config/config')
var ERROR = require('config/message').error
var MSG = require('config/message').msg

router.post('/login', function (req, res, next) {
  User.findOne({
    username: req.body.username
  }, function (err, user) {
    if (err) {
      return next(err)
    }

    if (!user) {
      return next(new Error(ERROR.ERROR_LOGIN_USER_NOT_FOUND))
    } else {
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          user.password = null
          var token = jwt.sign(user.toObject(), config.secret, {
            expiresIn: 60 * 5
          })
          res.json({ token })
        } else {
          return next(new Error(ERROR.ERROR_LOGIN_WRONG_PASSWORD))
        }
      })
    }
  })
})

router.post('/signup', function (req, res, next) {
  var username = req.body.username
  var password = req.body.password
  if (!username || !password) {
    return next(new Error(ERROR.ERROR_SIGNUP_MISSING_PARAMETERS))
  } else {
    var newUser = new User({
      username,
      password
    })

    newUser.save(function (err, newUser) {
      err
      ? next(err)
      : res.json({ msg: MSG.SIGNUP_SUCCESSFULLY })
    })
  }
})

module.exports = router
