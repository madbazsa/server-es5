// Dependencies
var express = require('express')
var router = new express.Router()

router.use('/auth', require('routes/auth.router'))
router.use('/', require('routes/basic.router'))

module.exports = router
