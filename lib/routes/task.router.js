// Dependencies
var modelName = 'task'
var express = require('express')
var router = new express.Router()

var Model = require('models/' + modelName + '.model')
var Controller = require('common/controller')

/* Get all. */
router.get('/', function (req, res, next) {
  Controller.findAll(Model, req, res, next)
})

/** Get one by id. */
router.get('/:id', function (req, res, next) {
  Controller.findOne(Model, req, res, next)
})

/** Create one. */
router.post('/', function (req, res, next) {
  Controller.save(Model, req, res, next)
})

/** Delete one by id. */
router.delete('/:id', function (req, res, next) {
  Controller.delete(Model, req, res, next)
})

/** Update one. */
router.put('/:id', function (req, res, next) {
  Controller.edit(Model, req, res, next)
})

module.exports = router
