// Dependencies
var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var morgan = require('morgan')
var cors = require('cors')
var config = require('config/config')
var auth = require('common/auth')
var app = express()

// MongoDB
mongoose.Promise = global.Promise

if (process.env.MONGO_ENV !== 'test') {
  mongoose.connect(config.database)
} else {
  mongoose.connect(config.database_gitlabci)
}

// Express
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(bodyParser.json())

// Morgan
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('dev'))
}

app.use(cors())

// Authorisation
app.use(auth.authorisation)

// Routes
app.use('/api', require('./routes'))

// Error handler
app.use(function (err, req, res, next) {
  res.status(500).send(err.errors || err.message)
})

// Start server
app.listen(8081, process.env.IP)
module.exports = app
