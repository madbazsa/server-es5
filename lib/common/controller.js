var ERROR = require('config/message').error

/** Get Collection size. */
var getCollectionSize = function (Model, next) {
  Model.count().exec(function (err, count) {
    if (err) {
      return next(err)
    }
    return count
  })
}

/* Get all. */
exports.findAll = function (Model, req, res, next) {
  var limit = null
  var skip = null
  var isParamsExist
  var isValidParams
  var fields = req.query.fields || ''
  fields = fields.replace(',', ', ')

  if (req.query.page && req.query.limit) {
    limit = parseInt(req.query.limit)
    skip = (parseInt(req.query.page) - 1) * limit
  }

  isParamsExist = (limit === null && skip === null)
  isValidParams = (limit > 0 && skip >= 0)

  if (isParamsExist || isValidParams) {
    Model.find()
      .select(fields)
      .sort(req.query.sort || 'taskname')
      .limit(limit || '')
      .skip(skip || '')
      .exec(function (err, data) {
        if (err) {
          return next(err)
        }
        var count = getCollectionSize(Model, next)
        var currentPage = parseInt(req.query.page)
        var lastPage = Math.ceil(count / limit)
        res.json({
          data,
          currentPage,
          lastPage
        })
      })
  } else {
    return next(new Error(ERROR.ERROR_PAGINATION))
  }
}

/** Get one by id. */
exports.findOne = function (Model, req, res, next) {
  var fields = req.query.fields || ''
  fields = fields.replace(',', ', ')
  Model.findOne({
    _id: req.params.id
  }, fields, function (err, result) {
    if (err) {
      return next(new Error(ERROR.ERROR_ENTRY_NOT_FOUND))
    }
    res.json(result)
  })
}

/** Create one. */
exports.save = function (Model, req, res, next) {
  var newItem = new Model(req.body)
  newItem.save(function (err, result) {
    if (err) {
      return next(err)
    } else {
      res.json(newItem)
    }
  })
}

/** Delete one by id. */
exports.delete = function (Model, req, res, next) {
  Model.remove({
    _id: req.params.id
  }, function (err, result) {
    if (err) {
      return next(new Error(ERROR.ERROR_DELETE))
    } else {
      res.status(200).send('OK')
    }
  })
}

/** Update one. */
exports.edit = function (Model, req, res, next) {
  // Find the entry by id.
  Model.findOne({
    _id: req.params.id
  }, function (err, result) {
    if (err) {
      return next(new Error(ERROR.ERROR_MODIFY_ENTRY_NOT_FOUND))
    }
    // Set the new properties.
    for (var prop in req.body) {
      if (req.body.hasOwnProperty(prop)) {
        result[prop] = req.body[prop]
      }
    }
    // save the entry
    result.save(function (err2) {
      if (err2) {
        return next(err2)
      } else {
        res.json(result)
      }
    })
  })
}
