var jwt = require('jsonwebtoken')
var _ = require('lodash')
var async = require('async')
var secret = require('config/config').secret
var ERROR = require('config/message').error
var PathRole = require('models/pathrole.model')
var User = require('models/user.model')

exports.authorisation = function (req, res, next) {
  async.waterfall([

    function parseUrl (cb) {
      var urlArr = []
      var url
      var path = ''
      url = req.url.indexOf('?') > 0 ? req.url.slice(1, req.url.indexOf('?')) : req.url.slice(1, req.url.length)
      urlArr = url.split('/')
      var checkForHexRegExp = new RegExp('^[0-9a-fA-F]{24}$')

      for (var i = 0, y = urlArr.length; i < y; i++) {
        path = checkForHexRegExp.test(urlArr[i]) ? path.concat('/:id') : path.concat('/' + urlArr[i])
      }
      cb(null, path)
    },

    function getPathRoles (path, cb) {
      PathRole.findOne({path: path, method: req.method}, function (err, pathRoles) {
        if (err) {
          cb(err)
        } else {
          // if roles is empty then we dont need authentication and authorisation
          pathRoles ? cb(null, pathRoles) : next()
        }
      })
    },

    function authentication (pathRoles, cb) {
      if (!req.headers.authorization) {
        return cb(new Error(ERROR.ERROR_TOKEN_NOT_EXIST))
      }
      jwt.verify(req.headers.authorization.slice(7), secret, function (err, u) {
        if (err) {
          return cb(new Error(ERROR.ERROR_TOKEN_NOT_VALID))
        }
        User.findById(u._id, function (err, user) {
          if (err) {
            return cb(err)
          }
          if (!user || user.username !== u.username) {
            return cb(new Error('Auth User not found'))
          } else {
            cb(null, pathRoles, user)
          }
        })
      })
    },

    function authorisation (pathRoles, user, cb) {
      _.intersection(pathRoles.roles, user.roles).length > 0
      ? cb()
      : cb(new Error(ERROR.ERROR_YOU_DONT_HAVE_ACCESS))
    }

  ], function (err, res) {
    err
    ? next(err)
    : next()
  })
}
