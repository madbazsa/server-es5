var mongoose = require('mongoose')
var Schema = mongoose.Schema

var pathRoleSchema = new Schema({
  path: {
    type: String,
    required: true
  },
  method: {
    type: String,
    required: true
  },
  roles: [String]
})

module.exports = mongoose.model('PathRole', pathRoleSchema)
