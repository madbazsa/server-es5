module.exports = {
  error: {
    ERROR_PAGINATION: 'Pagination error',
    ERROR_ENTRY_NOT_FOUND: 'Entry not found',
    ERROR_DELETE: 'Delete failed',
    ERROR_MODIFY_ENTRY_NOT_FOUND: 'Entry not found to modify error',
    ERROR_LOGIN_USER_NOT_FOUND: 'Authentication failed, User not found',
    ERROR_LOGIN_WRONG_PASSWORD: 'Authenticaton failed, wrong password.',
    ERROR_SIGNUP_MISSING_PARAMETERS: 'Enter all values.',
    ERROR_YOU_DONT_HAVE_ACCESS: 'You dont has access',
    ERROR_TOKEN_NOT_VALID: 'Token not valid',
    ERROR_TOKEN_NOT_EXIST: 'Token not exist'
  },
  msg: {
    SIGNUP_SUCCESSFULLY: 'Signup successfully'
  }
}
