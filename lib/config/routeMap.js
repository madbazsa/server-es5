module.exports = [
  { path: 'tasks', model: 'task', mainField: 'taskname' },
  { path: 'users', model: 'user', mainField: 'username' },
  { path: 'pathroles', model: 'pathrole', mainField: 'path' }
]
