process.env.NODE_ENV = 'test'
var async = require('async')
var chai = require('chai')
var chaiHttp = require('chai-http')
var should = chai.should()
var expect = chai.expect
var server = require('index')
var ERROR = require('config/message').error
var PathRole = require('models/pathrole.model')
var routes = require('config/routeMap')

chai.use(chaiHttp)

routes.map(function (route) {
  // import the model
  var Model = require('models/' + route.model + '.model')
  var newEntry = {}
  // Get the required fields from the model
  var reqFields = []
  Model.schema.eachPath(function (path) {
    if (Model.schema.paths[path].isRequired) {
      reqFields.push({ 'fieldName': path, 'type': Model.schema.paths[path].instance })
    }
  })

  function testSize (size, next) {
    Model.count({}, function (err, c) {
      if (err) { return next(err) }
      c.should.be.eql(size)
      next()
    })
  }

  describe(route.model.toUpperCase() + ' - basic rest test', function () {
    var newid

    before(function (done) {
      async.waterfall([

        function removeAllEntry (next) {
          Model.remove({}, function (err, res) {
            if (err) {
              return next(err)
            } else {
              next()
            }
          })
        },

        function removeAllPathRole (next) {
          PathRole.remove({}, function (err, res) {
            if (err) {
              return next(err)
            } else {
              next()
            }
          })
        },
        // Add some data with required and mainField data
        function addDummy (next) {
          var newEntry = {}
          var isMainFieldReqired = false
          for (var i = 1, x = 16; i < x; i++) {
            reqFields.map(function (field) {
              if (route.mainField === field.fieldName) {
                isMainFieldReqired = true
              }
              if (field.type === 'String') {
                newEntry[field.fieldName] = field.fieldName + i
              }
              if (field.type === 'Number') {
                newEntry[field.fieldName] = i
              }
              if (field.type === 'Boolean') {
                newEntry[field.fieldName] = false
              }
              if (field.type === 'Date') {
                newEntry[field.fieldName] = Date.now()
              }
            })
            if (!isMainFieldReqired) {
              newEntry[route.mainField] = route.mainField + i
            }
            Model.create(newEntry)
          }
          next()
        }

      ], function (err, res) {
        err ? done(err) : done()
      })
    })

    describe('- Create:', function () {
      it('Should get back the new entry, if everything went well', function (done) {
        var isMainFieldReqired = false
        var newEntry = {}
        reqFields.map(function (field) {
          if (route.mainField === field.fieldName) {
            isMainFieldReqired = true
          }
          if (field.type === 'String') {
            newEntry[field.fieldName] = field.fieldName + 50
          }
          if (field.type === 'Number') {
            newEntry[field.fieldName] = 50
          }
          if (field.type === 'Boolean') {
            newEntry[field.fieldName] = false
          }
          if (field.type === 'Date') {
            newEntry[field.fieldName] = Date.now()
          }
        })
        if (!isMainFieldReqired) {
          newEntry[route.mainField] = route.mainField + 50
        }
        chai.request(server)
          .post('/api/' + route.path)
          .send(newEntry)
          .end(function (err, res) {
            if (err) { return done(err) }
            newid = res.body._id
            should.not.exist(err)
            res.body[route.mainField].should.be.eql(route.mainField + 50)
            done()
          })
      })

      it('Should get error if required fields missed', function (done) {
        if (reqFields.length < 1) {
          done()
        } else {
          var reqField = reqFields.pop()
          reqFields.map(function (field) {
            if (field.type === 'String') {
              newEntry[field.fieldName] = field.fieldName + 50
            }
            if (field.type === 'Number') {
              newEntry[field.fieldName] = 50
            }
            if (field.type === 'Boolean') {
              newEntry[field.fieldName] = false
            }
            if (field.type === 'Date') {
              newEntry[field.fieldName] = Date.now()
            }
          })
        }
        chai.request(server)
          .post('/api/' + route.path)
          .send(newEntry)
          .end(function (err, res) {
            if (!err) { return done(err) }
            JSON.parse(res.error.text)[reqField.fieldName].message.should.be.eql('Path `' + reqField.fieldName + '` is required.')
            done()
          })
      })
    })

    describe('- Get:', function () {
      it('Should get the entry if the entry is exist in db', function (done) {
        chai.request(server)
          .get('/api/' + route.path + '/' + newid)
          .end(function (err, res) {
            if (err) { return done(err) }
            should.not.exist(err)
            res.body[route.mainField].should.be.eql(route.mainField + 50)
            done()
          })
      })

      it('Should get the entry with the given fields', function (done) {
        chai.request(server)
          .get('/api/' + route.path + '/' + newid + '?fields=' + route.mainField)
          .end(function (err, res) {
            if (err) { return done(err) }
            should.not.exist(err)
            res.body[route.mainField].should.be.eql(route.mainField + 50)
            Object.keys(res.body).length.should.be.eql(2)
            done()
          })
      })

      it('Should get error if the entry is NOT exist in db', function (done) {
        chai.request(server)
          .get('/api/' + route.path + '/invalid')
          .end(function (err, res) {
            should.exist(err)
            res.error.text.should.be.eql(ERROR.ERROR_ENTRY_NOT_FOUND)
            done()
          })
      })
    })

    describe('- Update:', function () {
      it('Should get back the updated entry, if everything went well', function (done) {
        async.waterfall([

          function put (next) {
            var updEntry = {}
            updEntry[route.mainField] = route.mainField + 100
            chai.request(server)
            .put('/api/' + route.path + '/' + newid)
            .send(updEntry)
            .end(function (err, res) {
              if (err) {
                return next(err)
              } else {
                should.not.exist(err)
                res.body[route.mainField].should.be.eql(route.mainField + 100)
                next(null, 16)
              }
            })
          },

          testSize,

          function testGetBack (next) {
            Model.findOne({
              _id: newid
            }, function (err, result) {
              if (err) { return next(err) }
              should.not.exist(err)
              result[route.mainField].should.be.eql(route.mainField + 100)
              next()
            })
          }

        ], function (err, res) {
          err ? done(err) : done()
        })
      })
      it('Should get error if required fields missed', function (done) {
        async.waterfall([

          function put (next) {
            var reqField = reqFields.pop()
            var updEntry = {}
            updEntry[route.mainField] = route.mainField + 100
            updEntry[reqField.fieldName] = null
            chai.request(server)
            .put('/api/' + route.path + '/' + newid)
            .send(updEntry)
            .end(function (err, res) {
              if (!err) {
                return next(err)
              } else {
                JSON.parse(res.error.text)[reqField.fieldName].message.should.be.eql('Path `' + reqField.fieldName + '` is required.')
                next(null, 16)
              }
            })
          },

          testSize

        ], function (err, res) {
          err ? done(err) : done()
        })
      })

      it('Should get error if the entry is NOT exist in db', function (done) {
        var updEntry = {}
        updEntry[route.mainField] = route.mainField + 100
        chai.request(server)
          .put('/api/' + route.path + '/invalid')
          .send(updEntry)
          .end(function (err, res) {
            should.exist(err)
            res.error.text.should.be.eql(ERROR.ERROR_MODIFY_ENTRY_NOT_FOUND)
            Model.count({}, function (err, c) {
              if (err) { return done(err) }
              c.should.be.eql(16)
              done()
            })
          })
      })
    })

    describe('- Delete:', function () {
      it('Should NOT get error if the entry is deleted', function (done) {
        chai.request(server)
          .delete('/api/' + route.path + '/' + newid)
          .end(function (err, res) {
            should.not.exist(err)
            Model.count({}, function (err2, c) {
              if (err2) { return done(err2) }
              c.should.be.eql(15)
              Model.findOne({
                _id: newid
              }, function (err3, result) {
                expect(result).to.be.null
                done()
              })
            })
          })
      })

      it('Should get error if the entry is NOT exist in db', function (done) {
        chai.request(server)
          .delete('/api/' + route.path + '/999999')
          .end(function (err, res) {
            should.exist(err)
            res.error.text.should.be.eql(ERROR.ERROR_DELETE)

            Model.count({}, function (err, c) {
              if (err) { return done(err) }
              c.should.be.eql(15)
              done()
            })
          })
      })
    })

    describe('- Pagination:', function () {
      it('Should get all the entries if everithing went well', function (done) {
        chai.request(server)
          .get('/api/' + route.path)
          .end(function (err, res) {
            should.not.exist(err)
            Model.count({}, function (err, c) {
              if (err) { return done(err) }
              c.should.be.eql(res.body.data.length)
              res.body.data[2][route.mainField].should.be.eql(route.mainField + 11)
              done()
            })
          })
      })

      it('Should get the filtered entries if pagination parameters exist and valid', function (done) {
        chai.request(server)
          .get('/api/' + route.path + '?sort=-' + route.mainField + '&limit=3&page=2&fields=_id,' + route.mainField)
          .end(function (err, res) {
            should.not.exist(err)
            res.body.data.length.should.be.eql(3)
            res.body.data[2][route.mainField].should.be.eql(route.mainField + 4)
            Object.keys(res.body.data[0]).length.should.be.eql(2)
            done()
          })
      })

      it('Should get error if the pagination params invalid', function (done) {
        chai.request(server)
          .get('/api/' + route.path + '?sort=-' + route.mainField + '&limit=invalid&page=invalid')
          .end(function (err, res) {
            should.exist(err)
            expect(res.body.data).to.be.undefined
            res.error.text.should.be.eql(ERROR.ERROR_PAGINATION)
            done()
          })
      })
    })
  })
})
