process.env.NODE_ENV = 'test'
var async = require('async')
var chai = require('chai')
var chaiHttp = require('chai-http')
var should = chai.should()
var expect = chai.expect
var server = require('index')
var ERROR = require('config/message').error
var Task = require('models/task.model')
var PathRole = require('models/pathrole.model')

chai.use(chaiHttp)

function testSize (size, next) {
  Task.count({}, function (err, c) {
    if (err) { return next(err) }
    c.should.be.eql(size)
    next()
  })
}

describe('TASK TEST', function () {
  var newid

  before(function (done) {
    async.waterfall([

      function removeAllTask (next) {
        Task.remove({}, function (err, res) {
          if (err) {
            return next(err)
          } else {
            next()
          }
        })
      },

      function removeAllPathRole (next) {
        PathRole.remove({}, function (err, res) {
          if (err) {
            return next(err)
          } else {
            next()
          }
        })
      },

      function addDummy (next) {
        var y = 15
        for (var i = 1, x = 16; i < x; i++) {
          Task.create({
            taskname: 'task' + i,
            description: 'description' + y--
          })
        }
        next()
      }

    ], function (err, res) {
      err ? done(err) : done()
    })
  })
  describe('- Create:', function () {
    it('Should get back the new entry, if everything went well', function (done) {
      chai.request(server)
        .post('/api/tasks')
        .send({
          taskname: 'task50',
          description: 'description50'
        })
        .end(function (err, res) {
          if (err) { return done(err) }
          newid = res.body._id
          should.not.exist(err)
          res.body.taskname.should.be.eql('task50')
          done()
        })
    })

    it('Should get error if required fields missed', function (done) {
      chai.request(server)
        .post('/api/tasks')
        .send({
          taskname: 'task50'
        })
        .end(function (err, res) {
          if (!err) { return done(err) }
          JSON.parse(res.error.text).description.message.should.be.eql('Path `description` is required.')
          done()
        })
    })
  })

  describe('- Get:', function () {
    it('Should get the entry if the entry is exist in db', function (done) {
      chai.request(server)
        .get('/api/tasks/' + newid)
        .end(function (err, res) {
          if (err) { return done(err) }
          should.not.exist(err)
          res.body.description.should.be.eql('description50')
          done()
        })
    })

    it('Should get the entry with the given fields', function (done) {
      chai.request(server)
        .get('/api/tasks/' + newid + '?fields=_id,description')
        .end(function (err, res) {
          if (err) { return done(err) }
          should.not.exist(err)
          res.body.description.should.be.eql('description50')
          expect(res.body.taskname).to.be.undefined
          done()
        })
    })

    it('Should get error if the entry is NOT exist in db', function (done) {
      chai.request(server)
        .get('/api/tasks/invalid')
        .end(function (err, res) {
          should.exist(err)
          res.error.text.should.be.eql(ERROR.ERROR_ENTRY_NOT_FOUND)
          done()
        })
    })
  })

  describe('- Update:', function () {
    it('Should get back the updated entry, if everything went well', function (done) {
      async.waterfall([

        function put (next) {
          chai.request(server)
          .put('/api/tasks/' + newid)
          .send({
            taskname: 'task100',
            description: 'description100'
          })
          .end(function (err, res) {
            if (err) {
              return next(err)
            } else {
              should.not.exist(err)
              res.body.description.should.be.eql('description100')
              next(null, 16)
            }
          })
        },

        testSize,

        function testGetBack (next) {
          Task.findOne({
            _id: newid
          }, function (err, result) {
            if (err) { return next(err) }
            should.not.exist(err)
            result.taskname.should.be.eql('task100')
            next()
          })
        }

      ], function (err, res) {
        err ? done(err) : done()
      })
    })

    it('Should get error if required fields missed', function (done) {
      async.waterfall([

        function put (next) {
          chai.request(server)
          .put('/api/tasks/' + newid)
          .send({
            taskname: 'task100',
            description: null
          })
          .end(function (err, res) {
            if (!err) {
              return next(err)
            } else {
              JSON.parse(res.error.text).description.message.should.be.eql('Path `description` is required.')
              next(null, 16)
            }
          })
        },

        testSize

      ], function (err, res) {
        err ? done(err) : done()
      })
    })

    it('Should get error if the entry is NOT exist in db', function (done) {
      chai.request(server)
        .put('/api/tasks/invalid')
        .send({
          taskname: 'task100',
          description: 'description100'
        })
        .end(function (err, res) {
          should.exist(err)
          res.error.text.should.be.eql(ERROR.ERROR_MODIFY_ENTRY_NOT_FOUND)
          Task.count({}, function (err, c) {
            if (err) { return done(err) }
            c.should.be.eql(16)
            done()
          })
        })
    })
  })

  describe('- Delete:', function () {
    it('Should NOT get error if the entry is deleted', function (done) {
      chai.request(server)
        .delete('/api/tasks/' + newid)
        .end(function (err, res) {
          should.not.exist(err)
          Task.count({}, function (err2, c) {
            if (err2) { return done(err2) }
            c.should.be.eql(15)
            Task.findOne({
              _id: newid
            }, function (err3, result) {
              expect(result).to.be.null
              done()
            })
          })
        })
    })

    it('Should get error if the entry is NOT exist in db', function (done) {
      chai.request(server)
        .delete('/api/tasks/999999')
        .end(function (err, res) {
          should.exist(err)
          res.error.text.should.be.eql(ERROR.ERROR_DELETE)

          Task.count({}, function (err, c) {
            if (err) { return done(err) }
            c.should.be.eql(15)
            done()
          })
        })
    })
  })

  describe('- Pagination:', function () {
    it('Should get all the entries if everithing went well', function (done) {
      chai.request(server)
        .get('/api/tasks')
        .end(function (err, res) {
          should.not.exist(err)
          Task.count({}, function (err, c) {
            if (err) { return done(err) }
            c.should.be.eql(res.body.data.length)
            res.body.data[2].taskname.should.be.eql('task11')
            done()
          })
        })
    })

    it('Should get the filtered entries if pagination parameters exist and valid', function (done) {
      chai.request(server)
        .get('/api/tasks?sort=-taskname&limit=3&page=2&fields=_id,taskname')
        .end(function (err, res) {
          should.not.exist(err)
          res.body.data.length.should.be.eql(3)
          res.body.data[2].taskname.should.be.eql('task4')
          expect(res.body.data[0].description).to.be.undefined
          done()
        })
    })

    it('Should get error if the pagination params invalid', function (done) {
      chai.request(server)
        .get('/api/tasks?sort=-description&limit=invalid&page=invalid')
        .end(function (err, res) {
          should.exist(err)
          expect(res.body.data).to.be.undefined
          res.error.text.should.be.eql(ERROR.ERROR_PAGINATION)
          done()
        })
    })
  })
})
