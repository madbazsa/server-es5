process.env.NODE_ENV = 'test'
var async = require('async')
var jwt = require('jsonwebtoken')
var chai = require('chai')
var chaiHttp = require('chai-http')
var should = chai.should()
var expect = chai.expect
var server = require('index')
var secret = require('config/config').secret
var MSG = require('config/message').msg
var ERROR = require('config/message').error
var User = require('models/user.model')
var Task = require('models/task.model')
var PathRole = require('models/pathrole.model')

chai.use(chaiHttp)
var token

describe('AUTH TEST', function () {
  before(function (done) {
    async.waterfall([

      function removeAllUser (next) {
        User.remove({}, function (err, res) {
          if (err) {
            return next(err)
          } else {
            next()
          }
        })
      },

      function removeAllTask (next) {
        Task.remove({}, function (err, res) {
          if (err) {
            return next(err)
          } else {
            next()
          }
        })
      },

      function removeAllPathRole (next) {
        PathRole.remove({}, function (err, res) {
          if (err) {
            return next(err)
          } else {
            next()
          }
        })
      },

      function addDummy (next) {
        var y = 15
        for (var i = 1, x = 16; i < x; i++) {
          Task.create({
            taskname: 'task' + i,
            description: 'description' + y--
          })
        }
        next()
      },

      function addDummyPathRole (next) {
        var pathRole1 = {
          path: '/api/tasks',
          method: 'GET',
          roles: ['admin', 'manager']
        }
        var pathRole2 = {
          path: '/api/tasks',
          method: 'POST',
          roles: ['admin']
        }
        PathRole.create(pathRole1)
        PathRole.create(pathRole2)
        next()
      },

      function addDummyUser (next) {
        var user1 = {
          username: 'admin',
          password: 'admin1',
          roles: ['customer', 'manager']
        }

        User.create(user1)
        next()
      }

    ], function (err, res) {
      err ? done(err) : done()
    })
  })

  describe('- Sign up:', function () {
    it('Should get an error if username or password are missing', function (done) {
      chai.request(server)
        .post('/api/auth/signup')
        .send({
          username: 'madbazsa'
        })
        .end(function (err, res) {
          if (!err) { return done(err) }
          res.error.text.should.be.eql(ERROR.ERROR_SIGNUP_MISSING_PARAMETERS)
          should.not.exist(res.body.msg)
          done()
        })
    })

    it('Should get confirm message if sign up was success', function (done) {
      chai.request(server)
        .post('/api/auth/signup')
        .send({
          username: 'madbazsa',
          password: 'madbazsa1'
        })
        .end(function (err, res) {
          if (err) { return done(err) }
          should.not.exist(res.error.text)
          res.body.msg.should.be.eql(MSG.SIGNUP_SUCCESSFULLY)
          done()
        })
    })
  })

  describe('- Login:', function () {
    it('Should get an error if Login user not exist in the db', function (done) {
      chai.request(server)
        .post('/api/auth/login')
        .send({
          username: 'notexist',
          password: 'madbazsa1'
        })
        .end(function (err, res) {
          if (!err) { return done(err) }
          res.error.text.should.be.eql(ERROR.ERROR_LOGIN_USER_NOT_FOUND)
          should.not.exist(res.body.token)
          done()
        })
    })

    it('Should get an error without token if the password was wrong', function (done) {
      chai.request(server)
        .post('/api/auth/login')
        .send({
          username: 'madbazsa',
          password: 'wrongpassword'
        })
        .end(function (err, res) {
          if (!err) { return done(err) }
          res.error.text.should.be.eql(ERROR.ERROR_LOGIN_WRONG_PASSWORD)
          should.not.exist(res.body.token)
          done()
        })
    })

    it('Should get a token if login was success', function (done) {
      chai.request(server)
        .post('/api/auth/login')
        .send({
          username: 'admin',
          password: 'admin1'
        })
        .end(function (err, res) {
          if (err) { return done(err) }
          should.exist(res.body.token)
          token = res.body.token
          expect(token.split('.')).to.have.length(3)
          jwt.verify(token, secret, function (err, u) {
            if (err) { should.not.exist(err) }
            u.username.should.be.eql('admin')
            done()
          })
        })
    })
  })

  describe('- Authorisation:', function () {
    it('Should get the results if token has access', function (done) {
      chai.request(server)
        .get('/api/tasks')
        .set('Authorization', 'Bearer ' + token)
        .end(function (err, res) {
          should.not.exist(err)
          res.body.data.length.should.be.eql(15)
          done()
        })
    })

    it('Should get error if token not exist and path is protected', function (done) {
      chai.request(server)
        .get('/api/tasks')
        .end(function (err, res) {
          should.exist(err)
          res.error.text.should.be.eql(ERROR.ERROR_TOKEN_NOT_EXIST)
          done()
        })
    })

    it('Should get error if token not valid', function (done) {
      chai.request(server)
        .get('/api/tasks')
        .set('Authorization', 'Bearer aaa.bbb.ccc')
        .end(function (err, res) {
          should.exist(err)
          res.error.text.should.be.eql(ERROR.ERROR_TOKEN_NOT_VALID)
          done()
        })
    })

    it('Should get error if token has no access', function (done) {
      chai.request(server)
        .post('/api/tasks')
        .send({
          taskname: 'test',
          description: 'test'
        })
        .set('Authorization', 'Bearer ' + token)
        .end(function (err, res) {
          should.exist(err)
          res.error.text.should.be.eql(ERROR.ERROR_YOU_DONT_HAVE_ACCESS)
          done()
        })
    })
  })
})
